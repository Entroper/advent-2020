use std::error::Error;
use std::vec::Vec;
use std::fs::File;
use std::io::{prelude::*, BufReader};

pub fn read_file() -> Result<Vec<u32>, Box<dyn Error>> {
	let file = File::open("input-1.txt")?;
	let reader = BufReader::new(file);

	let mut vec: Vec<u32> = Vec::new();
	for line in reader.lines() {
		let number: u32 = line?.parse()?;
		vec.push(number);
	}

	Ok(vec)
}

pub fn part1(vec: &Vec<u32>) {
	for i in 0..vec.len() - 1 {
		for j in i + 1..vec.len() {
			if vec[i] + vec[j] == 2020 {
				println!("{}", vec[i] * vec[j]);
			}
		}
	}
}

pub fn part2(vec: &Vec<u32>) {
	for i in 0..vec.len() - 2 {
		for j in i + 1..vec.len() - 1 {
			for k in j + 1..vec.len() {
				if vec[i] + vec[j] + vec[k] == 2020 {
					println!("{}", vec[i] * vec[j] * vec[k]);
				}	
			}
		}
	}
}
