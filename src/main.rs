use std::error::Error;
#[macro_use] extern crate lazy_static;

mod day1;
mod day2;
mod day3;
mod day4;

fn main() -> Result<(), Box<dyn Error>> {
	let vec = day1::read_file()?;
	day1::part1(&vec);
	day1::part2(&vec);

	let vec = day2::read_file()?;
	let part1 = day2::part1(&vec);
	let part2 = day2::part2(&vec);
	println!("{}, {}", part1, part2);

	let vec = day3::read_file()?;
	let trees = day3::part1(&vec, 1, 3);
	println!("{}", trees);
	let trees = day3::part2(&vec);
	println!("{}", trees);

	let vec = day4::read_file()?;
	let valid = day4::part1(&vec);
	println!("{}", valid);
	let valid = day4::part2(&vec);
	println!("{}", valid);

	Ok(())
}
