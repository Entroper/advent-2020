use std::error::Error;
use std::vec::Vec;
use std::fs::File;
use std::io::{prelude::*, BufReader};

pub fn read_file() -> Result<Vec<Vec<char>>, Box<dyn Error>> {
	let file = File::open("input-3.txt")?;
	let reader = BufReader::new(file);

	let mut vec: Vec<Vec<char>> = Vec::new();
	for line in reader.lines() {
		vec.push(line?.chars().collect());
	}

	Ok(vec)
}

pub fn part1(vec: &Vec<Vec<char>>, row_step: usize, col_step: usize) -> u32 {
	let mut i = 0;
	let mut j = 0;
	let mut trees = 0;
	while i < vec.len() {
		if vec[i][j] == '#' {
			trees += 1;
		}

		j = (j + col_step) % vec[i].len();
		i += row_step;
	}

	trees
}

pub fn part2(vec: &Vec<Vec<char>>) -> u32 {
	let mut answer = 1;
	answer *= part1(&vec, 1, 1);
	answer *= part1(&vec, 1, 3);
	answer *= part1(&vec, 1, 5);
	answer *= part1(&vec, 1, 7);
	answer *= part1(&vec, 2, 1);

	answer
}
