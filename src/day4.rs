use std::error::Error;
use std::vec::Vec;
use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::collections::HashMap;

extern crate regex;
use regex::Regex;

pub fn read_file() -> Result<Vec<HashMap<String, String>>, Box<dyn Error>> {
	let file = File::open("input-4.txt")?;
	let reader = BufReader::new(file);

	let mut vec: Vec<HashMap<String, String>> = Vec::new();
	let mut passport: HashMap<String, String> = HashMap::new();
	for line in reader.lines() {
		let line = line?;
		if line == "" {
			vec.push(passport);
			passport = HashMap::new();
		}
		else {
			for kvp in line.split_whitespace() {
				let kvp = kvp.to_string();
				let mut sides = kvp.split(":");
				let key = sides.next().ok_or("no key!")?;
				let val = sides.next().ok_or("no val!")?;
				passport.insert(key.to_string(), val.to_string());
			}
		}
	}
	if passport.len() > 0 {
		vec.push(passport);
	}

	Ok(vec)
}

pub fn part1(vec: &Vec<HashMap<String, String>>) -> usize {
	vec.iter().filter(part1_valid).count()
}

fn part1_valid(passport: &&HashMap<String, String>) -> bool {
	passport.contains_key("byr") &&
	passport.contains_key("iyr") &&
	passport.contains_key("eyr") &&
	passport.contains_key("hgt") &&
	passport.contains_key("hcl") &&
	passport.contains_key("ecl") &&
	passport.contains_key("pid")
}

pub fn part2(vec: &Vec<HashMap<String, String>>) -> usize {
	vec.iter().filter(|p| part2_valid(p).unwrap_or(false)).count()
}

fn part2_valid(passport: &&HashMap<String, String>) -> Option<bool> {
	let byr = passport.get("byr")?;
	let byr = byr.parse::<u32>().ok()?;
	if byr < 1920 || byr > 2002 {
		return Some(false)
	}

	let iyr = passport.get("iyr")?;
	let iyr = iyr.parse::<u32>().ok()?;
	if iyr < 2010 || iyr > 2020 {
		return Some(false)
	}

	let eyr = passport.get("eyr")?;
	let eyr = eyr.parse::<u32>().ok()?;
	if eyr < 2020 || eyr > 2030 {
		return Some(false)
	}

	lazy_static! {
		static ref HGT_REGEX: Regex = Regex::new(r"^(\d+)(in|cm)$").unwrap();
	}
	let hgt = passport.get("hgt")?;
	let captures = HGT_REGEX.captures(hgt)?;
	let measure = captures[1].parse::<u32>().ok()?;
	if !match &captures[2] {
		"cm" => measure >= 150 && measure <= 193,
		"in" => measure >= 59  && measure <= 76,
		_ => false
	} {
		return Some(false)
	}

	lazy_static! {
		static ref HCL_REGEX: Regex = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
	}
	let hcl = passport.get("hcl")?;
	if !HCL_REGEX.is_match(hcl) {
		return Some(false)
	}

	let ecl = passport.get("ecl")?;
	if
		ecl != "amb" &&
		ecl != "blu" &&
		ecl != "brn" &&
		ecl != "gry" &&
		ecl != "grn" &&
		ecl != "hzl" &&
		ecl != "oth" {
		return Some(false)
	}

	lazy_static! {
		static ref PID_REGEX: Regex = Regex::new(r"^[0-9]{9}$").unwrap();
	}
	let pid = passport.get("pid")?;
	if !PID_REGEX.is_match(pid) {
		return Some(false)
	}

	Some(true)
}
