use std::error::Error;
use std::vec::Vec;
use std::fs::File;
use std::io::{prelude::*, BufReader};

extern crate regex;
use regex::Regex;

pub struct PasswordInput {
	min: usize,
	max: usize,
	character: char,
	password: String
}

pub fn read_file() -> Result<Vec<PasswordInput>, Box<dyn Error>> {
	lazy_static! {
		static ref RE: Regex = Regex::new(r"(\d+)-(\d+) ([a-z]): ([a-z]+)").unwrap();
	}

	let file = File::open("input-2.txt")?;
	let reader = BufReader::new(file);

	let mut vec: Vec<PasswordInput> = Vec::new();
	for line in reader.lines() {
		let line = line?;
		let captures = RE.captures(line.as_str()).ok_or("no password input match")?;

		let input = PasswordInput {
			min: captures[1].parse()?,
			max: captures[2].parse()?,
			character: captures[3].chars().next().ok_or("no password character match")?,
			password: captures[4].to_string()
		};
		vec.push(input);
	}

	Ok(vec)
}

pub fn part1(vec: &Vec<PasswordInput>) -> usize {
	vec.iter().filter(|input| {
		let count = input.password.chars().filter(|c| *c == input.character).count();
		input.min <= count && count <= input.max
	}).count()
}

pub fn part2(vec: &Vec<PasswordInput>) -> usize {
	vec.iter().filter(|input| {
		let min = input.min - 1;
		let max = input.max - 1;
		let min_match = input.password.chars().nth(min).unwrap_or('0') == input.character;
		let max_match = input.password.chars().nth(max).unwrap_or('0') == input.character;
		min_match && !max_match || max_match && !min_match
	}).count()
}
